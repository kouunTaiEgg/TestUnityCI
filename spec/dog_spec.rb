require 'simplecov'
SimpleCov.start

class Dog
  def bark
    'Woof, woof!'
  end
end

describe Dog do
  it 'barks' do
    expect(subject.bark).to eq 'Woof, woof!'
  end
end